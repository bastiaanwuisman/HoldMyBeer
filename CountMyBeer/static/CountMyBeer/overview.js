'use strict';
// Global vars
let users = {};
let products = {};
let payments = [];
let id_name_map = new Map();

const template_user_column = $('<div />', {class: 'col-sm-6 col-md-6 col-lg-4 p-3'});
const template_user_card = $('<div />', {class: 'card'});
const template_user_card_header = $('<div />', {class: 'card-header'});
const template_user_card_nav = $('<ul />', {class: 'nav nav-tabs card-header-tabs', role: 'tablist'});
const template_user_card_nav_list = $('<li />', {class: 'nav-item'});
const template_user_card_nav_title_active = $('<a />', {class: 'nav-link active', role: 'tab', 'aria-selected': 'true'});
const template_user_card_nav_title = $('<a />', {class: 'nav-link', role: 'tab', 'aria-selected': 'false'});
const template_user_card_body = $('<div />', {class: 'card-body'});
const template_user_card_body_title = $('<div />', {class: 'card-title'});
const template_user_card_tab_content = $('<div />', {class: 'tab-content mt-3'});
const template_user_card_tab_active = $('<div />', {class: 'tab-pane active', role: 'tabpanel'});
const template_user_card_tab = $('<div />', {class: 'tab-pane', role: 'tabpanel'});
const template_user_card_tab_data = $('<p />', {class: 'card-text'});

const template_form = $('<form />');
const template_form_row = $('<div />', {class: 'form-group row'});
const template_form_row_label = $('<label />', {class: 'col-auto col-form-label'});
const template_form_row_col = $('<div />', {class: 'col-sm-8'});
const template_form_row_input = $('<input />', {class: 'form-control', type: 'number', min: '0', step: '0.01'});
const template_form_row_button_submit = $('<button />', {class: 'btn btn-primary', type: 'submit'});


function create_card(name, id, amount_owed) {

    let e_column = template_user_column.clone();
    let e_card = template_user_card.clone();
    let e_header = template_user_card_header.clone();
    let e_nav = template_user_card_nav.clone();
    let e_nav_list_overview = template_user_card_nav_list.clone();
    let e_nav_list_money = template_user_card_nav_list.clone();
    let e_nav_title_overview = template_user_card_nav_title_active.clone();
    let e_nav_title_money = template_user_card_nav_title.clone();
    let e_body = template_user_card_body.clone();
    let e_body_title = template_user_card_body_title.clone();
    let e_tab_content = template_user_card_tab_content.clone();
    let e_tab_overview = template_user_card_tab_active.clone();
    let e_tab_money = template_user_card_tab.clone();
    let e_tab_overview_data = template_user_card_tab_data.clone();

    let e_form = template_form.clone();
    let e_form_row = template_form_row.clone();
    let e_form_row_submit = template_form_row.clone();
    let e_form_row_label = template_form_row_label.clone();
    let e_form_row_col = template_form_row_col.clone();
    let e_form_row_col_submit = template_form_row_col.clone();
    let e_form_row_input = template_form_row_input.clone();
    let e_form_row_button_submit = template_form_row_button_submit.clone();

    e_form_row_button_submit.click(send_payment);
    e_form_row_button_submit.attr('id', id);
    e_form_row_button_submit.text('Settle');
    e_form_row_col_submit.append(e_form_row_button_submit);
    e_form_row_submit.append(e_form_row_col_submit);

    e_form_row_input.attr('id', 'inputSettleAmount' + id);
    e_form_row_input.attr('value', amount_owed);
    e_form_row_col.append(e_form_row_input);
    e_form_row_label.attr('for', 'inputSettleAmount' + id);
    e_form_row_label.text('Amount');
    e_form_row.append(e_form_row_label);
    e_form_row.append(e_form_row_col);

    e_form.append(e_form_row);
    e_form.append(e_form_row_submit);

    e_nav.attr('id', id);
    e_nav_title_overview.attr('href', '#overview'+id);
    e_nav_title_overview.attr('aria-controls', 'overview'+id);
    e_nav_title_overview.text('Overview');
    e_nav_title_money.attr('href', '#money'+id);
    e_nav_title_money.attr('aria-controls', 'money'+id);
    e_nav_title_money.text('Settle');

    e_body_title.text(name);
    e_tab_overview.attr('id', 'overview'+id);
    e_tab_money.attr('id', 'money'+id);

    e_tab_overview_data.attr('id', 'dept' + id);
    e_tab_overview_data.text('Dept: €' + amount_owed);

    e_tab_overview.append(e_tab_overview_data);
    e_tab_money.append(e_form);
    e_tab_content.append(e_tab_overview);
    e_tab_content.append(e_tab_money);
    e_body.append(e_body_title);
    e_body.append(e_tab_content);

    e_nav_list_overview.append(e_nav_title_overview);
    e_nav_list_money.append(e_nav_title_money);
    e_nav.append(e_nav_list_overview);
    e_nav.append(e_nav_list_money);
    e_header.append(e_nav);

    e_card.append(e_header);
    e_card.append(e_body);

    e_column.append(e_card);

    return e_column;
}


$(document).ready(function () {
    setup_ajax();
    get_data();
});


function setup_ajax() {
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
            }
        }
    });
}


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


function send_payment(e) {
    let id = parseInt(e.target.id);
    let amount = $('#inputSettleAmount' + id).val();

    let data = {
        'amount': amount,
        'user': id
    };

    $.ajax({
        type: 'POST',
        url: '/api/v1/create_payment/',
        data: data,
        success: function (data) {
            payments.push(data);
            // Update amount
            update_dept(id);
        },
        fail: function () {
            console.log('failed getting payment data');
        }
    });

    // Return false to prevent actual form submission :')
    return false;
}


function update_dept(id) {
    let e_dept_text = $('#dept' + id);
    let e_amount = $('#inputSettleAmount' + id);
    let dept = calculate_total_cost(id);
    e_dept_text.text('Dept: €' + dept);
    e_amount.val(dept);
}


function process_generic(data) {
    // First clear users, just in case
    let generic = {};
    // Add users indexed by their id, id is unique
    for (let i = 0; i < data.length; i++) {
        generic[data[i].id] = data[i];
    }

    return generic;
}


function process_users(data) {
    users = process_generic(data);
}


function process_products(data) {
    // products = process_generic(data);
    products = data;
}


function populate_initial() {
    sort_users();
    let user_row = $('#user_row');

    for (const [key, value] of id_name_map.entries()) {
        // const prod = products[key].product;
        user_row.append(create_card(value, key, calculate_total_cost(parseInt(key))));

        $('#' + key + ' a').on('click', function (e) {
            e.preventDefault();
            $(this).tab('show')
        });
    }
}


function calculate_total_cost(user_id) {
    let total = 0;
    for (let i = 0; i < products.length; i++) {
        let product = products[i];
        if (product.user === user_id) {
            total += product.amount * product.product.price;
        }
    }
    for (let i = 0; i < payments.length; i++) {
        let payment = payments[i];
        if (payment.user === user_id) {
            total -= payment.amount;
        }
    }

    // Do a quick round to prevent floating point errors from making 0 become -0
    total = Math.round(total * 100) / 100;

    return total.toFixed(2);
}


function sort_users() {
    // Create temporary array with the mappings
    let a = [];
    for (let k in users) {
        a.push([k, users[k].username]);
    }

    // Sort the array on the name of the product
    a.sort((a, b) => a[1].localeCompare(b[1]));

    // Finally create the new sorted map
    id_name_map = new Map(a);
}



function get_data() {
    get_users()
        .then(get_user_products)
        .then(get_payments)
        .then(populate_initial)
}


function get_payments() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/all_payments',
        success: function (data) {
            payments = data;
        },
        fail: function () {
            console.log('failed getting payment data');
        }
    })
}


function get_users() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/user',
        success: function (data) {
            process_users(data);
        },
        fail: function () {
            console.log('failed getting user data');
        }
    })
}


function get_user_products() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/overview_user_product',
        success: function (data) {
            process_products(data);
        },
        fail: function () {
            console.log('failed getting overview user products');
        }
    })
}
