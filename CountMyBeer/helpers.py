"""
Little helper file with useful small functions
"""
from CountMyBeer.models import Product, UserHasProduct, User


def generate_user_has_products(user: User) -> None:

    # Generate all UserHasProducts
    products = Product.objects.all()

    for product in products:
        UserHasProduct.objects.get_or_create(user=user, product=product)
